const DNA = require('../models/DNA');
class StatsServices {
    async actualStats(res){
        const respuesta = {};
        try {
            const mutants = await DNA.countDocuments({mutant: true});
            const noMutants = await DNA.countDocuments({mutant: false});
            const ratio = noMutants > 0 ? mutants / noMutants : 0;
            respuesta.count_mutations = mutants;
            respuesta.count_no_mutation = noMutants;
            respuesta.ratio = ratio;
            respuesta.success = true;
            res.status = 200;
        } catch (e) {
            respuesta.error = e.message;
            respuesta.success = false;
            res.status = 500;
        } finally {
            return respuesta;
        }
    }
}
module.exports = StatsServices;
