const config = require('../configuration');
const dna_pattern = require('../models/patterns/dna_pattern');
const DNA = require('../models/DNA');

const patternRestrict = new RegExp(dna_pattern.patternRestrict);
const patternMutation = new RegExp(dna_pattern.patternMutation);

class MutationServices {
    async isMutation({dna}, res) {
        const validation = this.validations(dna);
        if (validation.success) {
            if (!this.horizontalMutation(dna)) {
                if (!this.verticalMutation(dna)) {
                    if (!this.obliqueRightMutation(dna)) {
                        if (!this.obliqueLeftMutation(dna))
                            return await this.successMutation(false, dna, res);
                        else
                            return await this.successMutation(true, dna, res);
                    } else
                        return await this.successMutation(true, dna, res);

                } else return await this.successMutation(true, dna, res);

            } else
                return await this.successMutation(true, dna, res);
        } else {
            res.status(403);
            return validation;
        }
    }

    validations(dna) {
        let response = {
            success: true,
            error: ''
        };
        if (dna) {
            if (!Array.isArray(dna)) {
                response.success = false;
                response.error += 'DNA is not an Array.';
            }

            if (!this.validateMinLength(dna)) {
                if(response.error.length>0)
                    response.error += '\n';
                response.success = false;
                response.error += 'DNA has not the minimum length.';
            }

            if (!this.validateLength(dna)) {
                if(response.error.length>0)
                    response.error += '\n';
                response.success = false;
                response.error += 'DNA has not the same length.';
            }
            if (!this.validateCharacters(dna)) {
                if(response.error.length>0)
                    response.error += '\n';
                response.success = false;
                response.error += 'DNA has different letters to A, T, C, G.';
            }
        }
        else {
            response.success = false;
            response.error += 'The request doesnt have DNA data.';
        }
        return response;
    }

    validateMinLength(dna) {
        const dnaLength = dna.length;
        return dnaLength >= config.minSequence;
    }

    validateLength(dna) {
        const dnaLength = dna.length;
        for (const dnaItem of dna) {
            if (dnaItem.length !== dnaLength) {
                return false;
            }
        }
        return true;
    }

    validateCharacters(dna) {
        for (const dnaItem of dna) {
            if (!patternRestrict.test(dnaItem)) {
                return false;
            }
        }
        return true;
    }

    hasMutation(dnaItem) {
        return patternMutation.test(dnaItem);
    }

    horizontalMutation(dna) {
        for (const dnaItem of dna) {
            if (this.hasMutation(dnaItem)) {
                return true;
            }
        }
        return false;
    }

    verticalMutation(dna) {
        let dnaItem;
        for (let i = 0; i < dna.length; i++) {
            dnaItem = '';
            for (let e = 0; e < dna.length; e++) {
                dnaItem += dna[e].charAt(i);
            }
            if (this.hasMutation(dnaItem)) {
                return true;
            }
        }
        return false;
    }

    obliqueRightMutation(dna) {
        let dnaItem;
        for (let x = config.minSequence - 1; x < dna.length; x++) {
            dnaItem = '';
            for (let y = 0; y <= x; y++) {
                let i = x - y;
                dnaItem += dna[i].charAt(y);
            }
            if (this.hasMutation(dnaItem)) {
                return true;
            }
        }
        for (let x = dna.length - 2; x >= config.minSequence - 1; x--) {
            dnaItem = '';
            for (let y = 0; y <= x; y++) {
                let i = x - y;
                dnaItem += dna[dna.length - y - 1].charAt(dna.length - i - 1);
            }
            if (this.hasMutation(dnaItem)) {
                return true;
            }
        }
        return false;
    }

    obliqueLeftMutation(dna) {
        let dnaItem;
        for (let x = 0; x < config.minSequence - 1; x++) {
            dnaItem = '';
            for (let y = dna.length - 1; y >= x; y--) {
                let i = Math.abs((x - y));
                dnaItem += dna[i].charAt(y);
            }
            if (this.hasMutation(dnaItem)) {
                return true;
            }
        }
        for (let x = config.minSequence - 2; x > 0; x--) {
            dnaItem = '';
            for (let y = dna.length - 1; y >= x; y--) {
                let i = Math.abs((x - y));
                dnaItem += dna[dna.length - i - 1].charAt(dna.length - y - 1);
            }
            if (this.hasMutation(dnaItem)) {
                return true;
            }
        }
        return false;
    }

    async successMutation(success, dna, res) {
        if (success)
            res.status(200);
        else
            res.status(403);

        const previewDNA = await DNA.find({dna: dna});
        if(previewDNA.length === 0) {
            let dnaItem = new DNA({
                dna: dna,
                mutant: success,
            });
            await dnaItem.save();
        }

        return {
            success: success,
            message: success ? 'DNA has a mutation.' : 'DNA has not a mutation.'
        }
    }
}

module.exports = MutationServices;
