const {createToken} = require('../middleware/session')

class AuthServices {
    async logIn({user, password}, res) {
        console.log("User", user);
        const respuesta = {};
        const data = {};
        try {
            data.user = user;
            data.token = createToken(user);
            respuesta.success = true;
            respuesta.data = data;
            res.status = 200;
        } catch (e) {
            respuesta.error = e.message;
            respuesta.success = false;
            res.status = 500;
        } finally {
            return respuesta;
        }
    }
}

module.exports = AuthServices;
