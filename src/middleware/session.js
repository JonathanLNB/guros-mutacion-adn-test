const jwt = require('jwt-simple');
const moment = require('moment');
const config = require('../configuration');

function isAuth(req, res, next) {
    if (!req.headers.authorization) {
        return res.status(401).send({success: false, error: 'Unauthorized'})
    }

    const token = req.headers.authorization.split(' ')[1]

    decodeToken(token)
        .then(async response => {
            req.user = response.user;
            next();
        })
        .catch(response => {
            res.status(response.status);
            res.send({success: false, error: response.message});
        });
}

function getUnauthorizedResponse(req, res) {
    return req.auth
        ? {success: false, error:'Credentials ' + req.auth.user + ':' + req.auth.password + ' rejected'}
        : {success: false, error:'Request without credentials'}
}

function createToken(usuario) {
    const payload = {
        iat: moment().unix(),
        user: usuario,
        exp: moment().add(12, 'hours').unix()
    }

    return jwt.encode(payload, config.SECRET_TOKEN)
}

function decodeToken(token) {
    const decoded = new Promise((resolve, reject) => {
        try {
            let payload = jwt.decode(token, config.SECRET_TOKEN, false)
            resolve(payload)
        } catch (err) {
            reject({
                status: 401,
                message: 'Expired Token'
            })
        }
    })

    return decoded
}

module.exports = {
    isAuth,
    getUnauthorizedResponse,
    createToken
};
