const express = require('express');
const app = express();
const mongoose = require("mongoose");
const cors = require('cors');
const config = require('./configuration');
const basicAuth = require('express-basic-auth')
const {notFound, errorHandler} = require('./middleware/handler');
const {isAuth, getUnauthorizedResponse} = require('./middleware/session');

const corsConfig = {
    origin: '*',
    optionsSuccessStatus: 200
};

mongoose.connect(config.database, {user: config.user, pass: config.password})
    .then(db => console.log("Base de datos conectada."))
    .catch(e => console.log("Error: ", e));

const apiAuth = require('./routes/AuthRoutes');
const apiMutation = require('./routes/MutationRoutes');
const apiStats = require('./routes/StatsRoutes');


app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors(corsConfig));

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use(function (req, res, next) {
    console.log('Request Type:', req.method);
    console.log('Request URL:', req.originalUrl);
    next();
});
app.use('/api/mutation', isAuth, apiMutation);
app.use('/api/stats', isAuth, apiStats);

app.use(basicAuth({
    users: config.users,
    unauthorizedResponse: getUnauthorizedResponse
}));

app.use("/api/login", apiAuth);

app.use(notFound);
app.use(errorHandler);

const server = app.listen(config.port, function () {
    console.log(`El Servidor inicio en el puerto ${server.address().port}`);
});
module.exports = app;
