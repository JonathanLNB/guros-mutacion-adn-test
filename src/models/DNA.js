const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const DNA = new Schema({
    dna: [String],
    mutant: Boolean,
    createAt: {type: Date, default: Date.now},
});

module.exports = mongoose.model("dna", DNA);
