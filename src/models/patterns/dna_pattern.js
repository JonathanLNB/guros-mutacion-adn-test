const config = require('../../configuration');

module.exports = {
    patternRestrict: '^[ATCG]*$',
    patternMutation: `^[ATGC]*((A[A]{${config.minSequence - 1}})|(T[T]{${config.minSequence - 1}})|(G[G]{${config.minSequence - 1}})|(C[C]{${config.minSequence - 1}}))[ATGC]*$`
}
