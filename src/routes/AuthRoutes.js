const express = require('express');
const router = express.Router();
const AuthService = require('../services/AuthServices');

router.post("/", async (req, res, next) => {
    let auth = new AuthService();
    console.log("Req", req.auth);
    let response = await auth.logIn(req.auth, res);
    res.send(response);
});

module.exports = router;
