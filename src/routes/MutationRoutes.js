const express = require('express');
const router = express.Router();
const MutationService = require('../services/MutationServices');

router.post("/", async (req, res, next) => {
    let mutation = new MutationService();
    let response = await mutation.isMutation(req.body, res);
    res.send(response);
});

module.exports = router;
