const express = require('express');
const router = express.Router();
const StatsService = require('../services/StatsServices');

router.get("/", async (req, res, next) => {
    let stats = new StatsService();
    let response = await stats.actualStats(res);
    res.send(response);
});

module.exports = router;
