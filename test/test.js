const server = require('../src/app');
const supertest = require('supertest');
const requestWithSupertest = supertest(server);
const patterns = require('./helpers/patterns')
let token;

describe('LogIn Endpoints', () => {
    it('POST api/login login without credentials', async () => {
        const res = await requestWithSupertest
            .post('/api/login')
        expect(res.status).toEqual(401);
        expect(res.type).toEqual(expect.stringContaining('json'));
        expect(res.body).toHaveProperty('success');
        expect(res.body.success).toEqual(false);
        expect(res.body).toHaveProperty('error');
        expect(res.body.error).toEqual('Request without credentials');
    });

    it('POST api/login login with invalid credentials', async () => {
        const res = await requestWithSupertest
            .post('/api/login')
            .auth('admin','supersecre')
        expect(res.status).toEqual(401);
        expect(res.type).toEqual(expect.stringContaining('json'));
        expect(res.body).toHaveProperty('success');
        expect(res.body.success).toEqual(false);
        expect(res.body).toHaveProperty('error');
        expect(res.body.error).toEqual('Credentials admin:supersecre rejected');
    });

    it('POST api/login login with valid credentials', async () => {
        const res = await requestWithSupertest
            .post('/api/login')
            .auth('admin','supersecret')
        expect(res.status).toEqual(200);
        expect(res.type).toEqual(expect.stringContaining('json'));
        expect(res.body).toHaveProperty('success');
        expect(res.body.success).toEqual(true);
        expect(res.body).toHaveProperty('data');
        expect(res.body.data).toHaveProperty('user');
        expect(res.body.data).toHaveProperty('token');
        expect(res.body.data.user).toEqual('admin');
        token = res.body.data.token;
    });

    it('POST api/stats without token', async () => {
        const res = await requestWithSupertest
            .get('/api/stats')
        expect(res.status).toEqual(401);
        expect(res.type).toEqual(expect.stringContaining('json'));
        expect(res.body).toHaveProperty('success');
        expect(res.body).toHaveProperty('error');
        expect(res.body.success).toEqual(false);
        expect(res.body.error).toEqual('Unauthorized');
    });

    it('POST api/stats with expired token', async () => {
        const res = await requestWithSupertest.get('/api/stats').set('Authorization', `Bearer ${patterns.expiredToken}`)
        expect(res.status).toEqual(401);
        expect(res.type).toEqual(expect.stringContaining('json'));
        expect(res.body).toHaveProperty('success');
        expect(res.body).toHaveProperty('error');
        expect(res.body.success).toEqual(false);
        expect(res.body.error).toEqual('Expired Token');
    });

});

describe('Stats Endpoints', () => {
    it('GET api/stats show the stats information at day', async () => {
        const res = await requestWithSupertest.get('/api/stats').set('Authorization', `Bearer ${token}`);
        expect(res.status).toEqual(200);
        expect(res.type).toEqual(expect.stringContaining('json'));
        expect(res.body).toHaveProperty('success')
        expect(res.body.success).toEqual(true)
        expect(res.body).toHaveProperty('count_mutations')
        expect(res.body).toHaveProperty('count_no_mutation')
        expect(res.body).toHaveProperty('ratio')
    });

});


describe('Mutation Endpoints', () => {

    it('POST api/mutation evaluate an empty request', async () => {
        const res = await requestWithSupertest.post('/api/mutation')
            .set('Authorization', `Bearer ${token}`)
            .send({});
        expect(res.status).toEqual(403);
        expect(res.type).toEqual(expect.stringContaining('json'));
        expect(res.body).toHaveProperty('success')
        expect(res.body.success).toEqual(false)
        expect(res.body).toHaveProperty('error')
        expect(res.body.error).toEqual('The request doesnt have DNA data.')
    });

    it('POST api/mutation evaluate the dna but is not an array', async () => {
        const res = await requestWithSupertest.post('/api/mutation')
            .set('Authorization', `Bearer ${token}`)
            .send({
                dna: "Hola"
            });
        expect(res.status).toEqual(403);
        expect(res.type).toEqual(expect.stringContaining('json'));
        expect(res.body).toHaveProperty('success')
        expect(res.body.success).toEqual(false)
        expect(res.body).toHaveProperty('error')
        expect(res.body.error).toEqual('DNA is not an Array.\nDNA has not the same length.\nDNA has different letters to A, T, C, G.')
    });

    it('POST api/mutation evaluate the dna string array with different letters and different length', async () => {
        const res = await requestWithSupertest.post('/api/mutation')
            .set('Authorization', `Bearer ${token}`)
            .send({
                dna: patterns.invalidDNA1
            });
        expect(res.status).toEqual(403);
        expect(res.type).toEqual(expect.stringContaining('json'));
        expect(res.body).toHaveProperty('success')
        expect(res.body.success).toEqual(false)
        expect(res.body).toHaveProperty('error')
        expect(res.body.error).toEqual('DNA has not the same length.\nDNA has different letters to A, T, C, G.')
    });


    it('POST api/mutation evaluate the dna string array with different length', async () => {
        const res = await requestWithSupertest.post('/api/mutation')
            .set('Authorization', `Bearer ${token}`)
            .send({
                dna: patterns.invalidDNA2
            });
        expect(res.status).toEqual(403);
        expect(res.type).toEqual(expect.stringContaining('json'));
        expect(res.body).toHaveProperty('success')
        expect(res.body.success).toEqual(false)
        expect(res.body).toHaveProperty('error')
        expect(res.body.error).toEqual('DNA has not the same length.')
    });

    it('POST api/mutation evaluate the dna string array with different letters', async () => {
        const res = await requestWithSupertest.post('/api/mutation')
            .set('Authorization', `Bearer ${token}`)
            .send({
                dna: patterns.invalidDNA3
            });
        expect(res.status).toEqual(403);
        expect(res.type).toEqual(expect.stringContaining('json'));
        expect(res.body).toHaveProperty('success')
        expect(res.body.success).toEqual(false)
        expect(res.body).toHaveProperty('error')
        expect(res.body.error).toEqual('DNA has different letters to A, T, C, G.')
    });

    it('POST api/mutation evaluate the dna string array without minimum length', async () => {
        const res = await requestWithSupertest.post('/api/mutation')
            .set('Authorization', `Bearer ${token}`)
            .send({
                dna: patterns.invalidDNA4
            });
        expect(res.status).toEqual(403);
        expect(res.type).toEqual(expect.stringContaining('json'));
        expect(res.body).toHaveProperty('success')
        expect(res.body.success).toEqual(false)
        expect(res.body).toHaveProperty('error')
        expect(res.body.error).toEqual('DNA has not the minimum length.')
    });

    it('POST api/mutation evaluate the dna string array with mutation', async () => {
        const res = await requestWithSupertest.post('/api/mutation')
            .set('Authorization', `Bearer ${token}`)
            .send({
                dna: patterns.mutanteDNA1
            });
        expect(res.status).toEqual(200);
        expect(res.type).toEqual(expect.stringContaining('json'));
        expect(res.body).toHaveProperty('success')
        expect(res.body.success).toEqual(true)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toEqual('DNA has a mutation.')
    });

    it('POST api/mutation evaluate the dna string array with mutation', async () => {
        const res = await requestWithSupertest.post('/api/mutation')
            .set('Authorization', `Bearer ${token}`)
            .send({
                dna: patterns.mutanteDNA2
            });
        expect(res.status).toEqual(200);
        expect(res.type).toEqual(expect.stringContaining('json'));
        expect(res.body).toHaveProperty('success')
        expect(res.body.success).toEqual(true)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toEqual('DNA has a mutation.')
    });

    it('POST api/mutation evaluate the dna string array with mutation', async () => {
        const res = await requestWithSupertest.post('/api/mutation')
            .set('Authorization', `Bearer ${token}`)
            .send({
                dna: patterns.mutanteDNA3
            });
        expect(res.status).toEqual(200);
        expect(res.type).toEqual(expect.stringContaining('json'));
        expect(res.body).toHaveProperty('success')
        expect(res.body.success).toEqual(true)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toEqual('DNA has a mutation.')
    });

    it('POST api/mutation evaluate the dna string array without mutation', async () => {
        const res = await requestWithSupertest.post('/api/mutation')
            .set('Authorization', `Bearer ${token}`)
            .send({
                dna: patterns.noMutanteDNA
            });
        expect(res.status).toEqual(403);
        expect(res.type).toEqual(expect.stringContaining('json'));
        expect(res.body).toHaveProperty('success')
        expect(res.body.success).toEqual(false)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toEqual('DNA has not a mutation.')
    });
});
