# Desafío Sr. Full Stack Developer

Requerimos que desarrolles un proyecto que detecte si una persona tiene diferencias genéticas basándose en
su secuencia de ADN. Para eso es necesario crear un programa con un método o función con la siguiente
firma:

```java
boolean hasMutation(String[] dna);
```
Debe recibir como parámetro un arreglo de cadena caracteres que representan cada fila de una tabla de NxN
con la secuencia del ADN. Las letras de los caracteres solo pueden ser: A, T, C, G; las cuales representan
cada base nitrogenada del ADN.

```bash
Sin Mutación
```

A T G C G A  |
------------ |
C A G T G C  |
T T A T T T  |
A G A C G G  |
G C G T C A  |
T C A C T G  |

```bash
Con Mutación
```
A T G C G A  |
------------ |
C A G T G C  |
T T A T G T  |
A G A A G G  |
C C C C T A  |
T C A C T G  |

Sabrás si existe una mutación si se encuentra más de una secuencia de cuatro letras iguales, de forma oblicua,
horizontal o vertical.

### Ejemplo de mutación

```java
String[] dna = { "ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA",
"TCACTG" };
```
En este caso el llamado a la función hasMutation(dna) devuelve true. Desarrolla el algoritmo de la manera
más eficiente posible.

## Desafíos
### Nivel 1
Programa (en cualquierlenguaje de programación) que cumpla con lo descrito anteriormente.

### Nivel 2
Crear una API REST, hostear esa API en un cloud computing libre (AWS, Azure DevOps, GCP, etc.), crear el
endpoint POST /mutation en donde se pueda detectar si existe mutación enviando la secuencia de ADN
mediante un JSON el cual tenga el siguiente formato:

```bash
POST /mutation
{
    "dna":["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"]
}
```
En caso de verificar una mutación, debería devolver un 200, en caso contrario un 403.

### Nivel 3

Anexar una base de datos, la cual guarde los ADNs verificados con la API. Sólo 1 registro por ADN. Exponer un
servicio extra GET /stats que devuelva un JSON con las estadísticas de las verificaciones de ADN:

```bash
{
    "count_mutations": 40,
    "count_no_mutation": 100,
    "ratio": 0.4
}
```

Tener en cuenta que la API puede recibirfluctuaciones agresivas de tráfico (entre 100 y 1 millón de peticiones
por segundo).

## Entregable
* Repositorio en GitLab del código
* README.md con lo necesario para ejecutarlo local
* URL de la API


## Requisitos
* Node JS (Desarrollado en v16.4.2)
* MongoDB (Desarrollado en 4.4.5)

## Configuración
Ingresamos al prompt de mongo y ejecutamos el siguiente script
```bash
use guros
use admin
```
En caso de que no este configurado es necesario crear un usuario admin con sus propias credenciales.

```bash
db.createUser(
  {
    user: "admin",
    pwd: "",
    roles: [ { role: "userAdminAnyDatabase", db: "admin" }, "readWriteAnyDatabase" ]
  }
)
```

Creamos el usuario de la base de datos

```bash
db.createUser(
    {
        user: "guros",
        pwd: "Guros_Test",
        roles: [{role: "readWrite", db: "guros"}]
    }
)
```

En caso de no tener configurado mongo para la recepción de autenticación accedemos al archivo mongod.conf dependiendo del sistema operativo como lo dice la documentación de [MongoDB](https://docs.mongodb.com/v4.4/reference/configuration-options/) (En mi caso fue usado macOS).

```bash
nano /usr/local/etc/mongod.conf 

#Agregamos la siguiente linea
security:
  authorization: enabled
```

Y reiniciamos el servicio de acuerdo al sistema operativo
```bash
brew services restart mongodb-community      
```

## Instalación
Una vez clonado el proyecto  accedemos a la carpeta:

```bash
cd guros-mutacion-adn-test
```

Seguido de esto hacemos la instalación del node_modules con el comando:
```bash
npm install
```
Para correr el proyecto ejecutamos:
```bash
npm run dev
```
Para ejecutar las pruebas usamos el comando:
```bash
npm test
```

